module.exports = {
    MONGODB_URI: process.env.MONGODB_URI || 'mongodb://localhost/senzopt',
    PORT: process.env.PORT || 3000,
}